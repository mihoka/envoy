FROM node:12.16.1-alpine

ARG type=unknown

WORKDIR /envoy
COPY . .
RUN apk --no-cache --virtual build-dependencies add \
    python \
    make \
    g++
RUN npm ci
RUN npm run build

RUN apk del \
    python \
    make \
    g++

STOPSIGNAL SIGTERM
ENTRYPOINT ["node", "./build/index.js"]