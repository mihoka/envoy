# Envoy

[This project](https://gitlab.com/mihoka/envoy) is a simple IRC client that will forward messages incoming from Many channels of Twitch towards a Queue (Likely AMQP) that will be processed by [Workers](https://gitlab.com/mihoka/worker). But wait, there is more! The Envoy will also take care of being a Writer, which means he will connect to our custom queue system and will send the messages to the various IRC Channels back to the user.

## What's required for this project to run?

First of all, internet. At least, for production use. We are connecting to the [Twitch IRC chat](https://dev.twitch.tv/docs/irc). For the test suites, it's (not yet, but will probably be) mocked.

You'll also need an instance of AMQP running where the messages will be sent. This AMQP connection will need to be provided as the environment variable `AMQP_STRING`. To spawn a new AMQP instance using docker locally, you can execute this command:

```bash
docker run -d --hostname envoy-amqp --name envoy-amqp -p 15672:15672 -p 5672:5672 rabbitmq:3-management
```

If you run the Writer Envoy as well, you'll need our custom queue to be up and running locally. This queue can be found in this repository. You'll then need to run it using docker, with the following command:

```bash
docker run -d --name mihoka-dispatcher -p 22085:22085 https://registry.gitlab.com/v2/mihoka/dispatcher --local
```