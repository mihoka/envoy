import { createLogger } from 'bunyan';

export const logger = createLogger({
  name: 'envoy',
  level: 'info',
  stream: process.stdout
});
