import SuperAgent from 'superagent';

export interface ChannelV2 {
  roomName: string;
  roomId: string;
  uuid: string;
}
export interface MihokaChannel {
  uuid?: string;
  id: string;
  name: string;
  display: string;
  kind?: string;
  is_active?: boolean;
  created_at?: string;
  updated_at?: string;
  deleted_at?: string;
}

export const getChannelsV2 = async (): Promise<ChannelV2[]> => {
  const channels = await SuperAgent.get(process.env.API_V2_URI || 'http://localhost:31625/channels');
  if (!channels.ok) {
    throw channels.error;
  }
  if (!channels.body || !channels.body.results || channels.body.results.length < 1) {
    throw Error('No body was returned');
  }
  return channels.body.results;
};

export const getChannels = async (): Promise<MihokaChannel[]> => {
  const channels = await SuperAgent.get(`${
    process.env.API_URI || 'http://localhost:8080'
  }/channels?where={"is_active":true}`)
  .set("x-access-token", process.env.API_TOKEN);
  if (!channels.ok) {
    throw channels.error;
  }
  if (!channels.body || !channels.body.data) {
    throw Error('No body was returned');
  }
  return channels.body.data;
}
