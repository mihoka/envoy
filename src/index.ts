import { Sentry } from "./sentry";
import { logger } from './logger';
import { getReader } from './reader';
// import { getWriter } from './writer';

process.on('uncaughtException', err => {
  logger.error(err);
  Sentry.captureException(err);
});

if (process.argv.includes('--reader')) {
  // This Envoy is a reader
  logger.info({
    type: 'reader'
  }, 'Preparing Envoy as a reader');
  getReader().then((reader) => {
    reader.onStart();
  }).catch((err) => {
    logger.fatal({
      err
    }, 'An error occured while reading messages');
  });
} else if (process.argv.includes('--writer')) {
  // This envoy is a writer!
  logger.info({
    type: 'writer'
  }, 'Preparing Envoy as a writer');
} else {
  logger.fatal({
    args: JSON.stringify(process.argv)
  }, 'Unable to start Envoy, neither the --reader or --writer parameter is found');
  process.exit(1);
}
