import { logger } from './logger';
import { connect, ChannelWrapper } from 'amqp-connection-manager';

export const queue = 'incoming';

export interface WriterOptions {
  uri?: string;
  queue?: string;
}

export const createWriter = (uri: string): ChannelWrapper => {
  logger.info({uri}, 'Connecting to the AMQP server');
  const conn = connect([uri || 'amqp://guest:guest@localhost']);
  logger.info('creating a new channel on AMQP server');
  return conn.createChannel();
}

export let amqpQueue: ChannelWrapper|undefined;

export const getAmqpWriter = (): ChannelWrapper => {
  if (!amqpQueue) {
    logger.info('Queue writer does not exists yet. Creating.')
    amqpQueue = createWriter(process.env.AMQP_STRING);
  }
  return amqpQueue;
};
