import { Client, IRC_TYPES, IParsedMessage } from 'twsc';
import { getAmqpWriter, queue } from './reader-queue';
import { getChannels, MihokaChannel } from './api';
import { logger } from './logger';
import { ChannelWrapper } from 'amqp-connection-manager';

export let reader: Client|undefined;
export let amqp: ChannelWrapper|undefined;

export const onReady = async (): Promise<void> => {
  let channels: MihokaChannel[]|undefined;
  try {
    channels = await getChannels();
  } catch (err) {
    console.log(err.stack);
    logger.fatal({err}, 'Failed to fetch the channel list from the server');
    process.exit(1);
  }
  for (const channel of channels) {
    logger.info({
      channel: channel.name,
      uuid: channel.uuid,
    }, 'Joining channel');
    amqp.addSetup(async amqpChannel => {
      await amqpChannel.assertQueue(channel.name, {durable: true});
    });
    reader.send(`JOIN #${channel.name}`);
  }
}

export const messageToString = (message: IParsedMessage): string => {
  return JSON.stringify({
    ...message,
    tags: Array.from(message.tags || []),
    prefix: Array.from(message.prefix || []),
  });
}

export const onMessage = async (msg): Promise<void> => {
  if ([
    IRC_TYPES.PRIVMSG,
    IRC_TYPES.NOTICE,
    IRC_TYPES.USERNOTICE
  ].includes(msg.type)) {
    logger.info({
      msg
    }, 'transporting elligible message to the queue');
    const message = messageToString(msg);
    amqp.sendToQueue(msg.params[0].substr(1), Buffer.from(message));
  }
}

export const getReader = async (): Promise<Client> => {
  reader = new Client({
    capabilities: [
      "twitch.tv/tags",
      "twitch.tv/membership",
      "twitch.tv/commands",
    ]
  });
  amqp = getAmqpWriter();
  reader.on('ready', onReady);
  reader.on('message', onMessage);
  return reader;
};
